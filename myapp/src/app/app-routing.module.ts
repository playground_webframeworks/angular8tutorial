import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './home/home.component'; // ADDED
import { ListComponent } from './list/list.component'; // ADDED

const routes: Routes = [
  { path: '', component: HomeComponent }, // ADDED
  { path: 'list', component: ListComponent } // ADDED
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
