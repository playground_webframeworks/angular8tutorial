import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  clickCounter: number = 0;
  name: string = '';

  constructor() { }

  ngOnInit() {
  }

  countClick() {
    this.clickCounter += 1;
  }

  getSyleClickCounter() {
    if (this.checkClickCounter()) {
      var styles = {
        'background-color': 'mistyrose',
        'border': '4px solid black'
      }
    } else {
      var styles = {
        'background-color': 'lightgray',
        'border': 'none'
      }
    }
    return styles;
  }

  setClasses() {
    let myClasses = {
      active: this.checkClickCounter(),
      notactive: !this.checkClickCounter(),
    };
    return myClasses;
  }

  checkClickCounter() {
    return this.clickCounter > 4 ? true : false;
  }

}

