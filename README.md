# angular8tutorial

## installing nvm, node, npm

### Option 1 (recommended) Installing nvm

* from https://tecadmin.net/install-nodejs-with-nvm/
* nvm manages different node installations on system
* can have differen versions installed

### Option 2: Getting nodejs 10.x (latest LTS as of writing 09.2019) on Ubuntu 18.04:

* this installs node / npm globally. not recommended, as you cna only have one version installed

```
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
sudo apt install nodejs
```

## start tutorial

https://coursetro.com/posts/code/174/Angular-8-Tutorial-&-Crash-Course







